package main

import (
	"fmt"
	"io/ioutil"
)

// Page ...
type Page struct {
	TiTle string
	Body  []byte
}

func (p *Page) save() error {
	filename := p.TiTle + ".txt"
	return ioutil.WriteFile(filename, p.Body, 0600)
}

func loadPage(title string) (*Page, error) {
	filename := title + ".txt"
	body, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	return &Page{TiTle: title, Body: body}, nil
}

func main() {
	p1 := &Page{TiTle: "TestPage", Body: []byte("这是中文")}
	p1.save()
	p2, _ := loadPage("TestPage")
	fmt.Println(string(p2.Body))
}
