package main

import (
	"fmt"
	"reflect"
	"sync"
)

//go语言支持对返回值进行命名
//方便调用者直到返回值的含义
//需要注意的是，若使用命名的方式返回参数，
//则所有返回参数都要命名，不能有的参数命名，
//而有的参数没命名
func nameReturn() (a, b int) {
	a = 1
	b = 2
	return a, b
}

//函数形参是值传递方式，但是有些数据类型是引用类型，
//比如切片（对连续内存的引用），指针，map对象等，
//这些数据类型作为函数参数实际上也是值传递，只是
//传递的是这些对象的指针

//切片作为参数，传递指针，在函数内部修改形参元素也会影响实参
func passSlice(s []int) []int {
	s[0] = 10
	return s
}

//数组作为形参
//前面讲过，Go语言中指针不能运算
//所以，使用数组传递参数时，不能在函数内部
//访问指针元素
func passArr(arr *[]int) *[]int {
	return arr
}

//延迟处理：
//延迟处理一般应用于对一些成对的操作，比如打开/关闭文件，互斥体加锁/解锁
//延迟处理把成对的操作交给系统处理，总是在函数返回时才执行
//延迟处理遵循栈的后进先出的原则，即最先被加入延迟队列的总是最后执行
var (
	// 一个演示用的映射
	valueByKey = make(map[string]int)
	// 保证使用映射时的并发安全的互斥锁
	valueByKeyGuard sync.Mutex
)

func readValue(key string) int {
	// 对共享资源加锁
	valueByKeyGuard.Lock()
	defer valueByKeyGuard.Unlock() //解锁，函数返回时才执行该语句
	// 取值
	v := valueByKey[key]
	// 返回值
	return v
}

//结构体
//Go语言没有类的概念，所有类的设计都是通过结构体来实现
//声明结构体
//没有构造函数
//结构体方法：即类的成员函数
//Go语言中通过定义"接收器"来模拟结构体方法
//接收器表明该方法的作用的对象，可以是结构体，也可以是内置的基本类型，具体如下
type Cat struct {
	Color string
	Name  string
}

//模拟构造函数
func createCatByName(name string) *Cat {
	return &Cat{
		Name:  name,
		Color: "",
	}
}

//接收器
func (c *Cat) setName(name string) (a string) {
	c.Name = name
	a = name
	return a
}

func main() {
	//键值循环：for range结构
	//需要要注意的是，val 始终为集合中对应索引的值拷贝，因此它一般只具有只读性质
	//对它所做的任何修改都不会影响到集合中原有的值

	// for k, v := range []int{1, 2, 3, 0} {
	// 	fmt.Println("Key: %d, Value: %d", k, v)
	// }

	slice1 := []int{1, 2, 3} //这是切片
	var arr [3]int

	fmt.Println("arr:", reflect.TypeOf(arr))     // 返回的是数组：[3]int
	fmt.Println("type:", reflect.TypeOf(slice1)) //返回的不是数组：[3]int
	fmt.Println(passSlice(slice1))               //实参在函数内部被改变

	var cat Cat
	fmt.Println(cat.setName("我么"), cat.Name)
}
