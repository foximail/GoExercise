package main

import "testing"

// TestHello ...
func TestHello(t *testing.T) {
	got := Hello("Hello")
	want := "Hello"
	if got != want {
		t.Errorf("got '%q', want '%q'", got, want)
	}
}
