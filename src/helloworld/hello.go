package main

import (
	"bufio"
	"fmt"
	"os"
	"runtime"
	"unsafe"
)

type MyInt int

func (q *MyInt) print() {
	fmt.Print("我没问题啊")
}

// Hello ...
func Hello(name string) string {
	return name
}

func main() {

	var a int
	a = 10.0

	var boil float32 = 100
	fmt.Print(boil)
	fmt.Println("www")

	var my MyInt
	my.print()

	// str1 := "我们"
	// str2 := "都是好孩子"
	// str3 := str1 + str2

	fmt.Println("OS: ", runtime.GOOS, "ARC: ", runtime.GOARCH)

	inputeReader := bufio.NewReader(os.Stdin)

	fmt.Println("请输入参数：")
	input, err := inputeReader.ReadString('\n')
	if err != nil {
		fmt.Println("输入有问题")
	} else {
		input = input[:len(input)-1]
		fmt.Println("Hello, ", input)
	}
	var a int = 10
	b := []int{1, 2, 3}
	fmt.Println("int类型大小：", unsafe.Sizeof(a), unsafe.Sizeof(b))
}
