package main

import (
	"fmt"
	"os"
	"strings"
)

// 命令行参数变量：os.Args
// 其实际上一个字符串切片类型
func main() {
	var s, sep string
	sep = " "
	for i := 0; i < len(os.Args); i++ {
		s = os.Args[i] + sep
	}

	fmt.Println(s)

	fmt.Println(strings.Join(os.Args[:], " "))
}
