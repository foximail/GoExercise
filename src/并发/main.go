package main

import (
	"fmt"
	"runtime"
	"sync"
	"time"
)

var (
	wg    sync.WaitGroup
	mutex = new(sync.Mutex)
	count int
)

func add(mut *sync.Mutex) {
	defer wg.Done()
	for i := 0; i < 1000; i++ {
		mutex.Lock()
		count++
		fmt.Println("add: ", count)
		mutex.Unlock()
		runtime.Gosched()
	}
}

func minu(mut *sync.Mutex) {
	defer wg.Done()

	for i := 0; i < 1000; i++ {
		mut.Lock()
		count--
		fmt.Println("minu: ", count)
		mut.Unlock()
		runtime.Gosched()
	}
}

func main() {

	// names := []string{"Eric", "Mark", "Hello", "Robert", "Jim"}
	// for _, name := range names {
	// 	go func(who string) {
	// 		fmt.Println("Hello, ", who)
	// 	}(name)
	// 	// time.Sleep(time.Millisecond)
	// }
	// time.Sleep(time.Millisecond)
	wg := new(sync.WaitGroup)
	wg.Add(1)
	timeout := 2 * time.Second
	timer := time.NewTicker(timeout) //计时器
	go func() {
		defer wg.Done()
		for {
			select {
			case exp := <-timer.C:
				{
					fmt.Println("绝对时间 ", exp)
				}
			default:
				{
					// fmt.Println("未到时间")
				}
			}

		}
	}()
	wg.Wait()
	// fmt.Println("当前时间：", time.Now())

	// exp1 := <-timer.C
	// fmt.Println("绝对时间 ", exp1)

	// any_chan := make(chan interface{}, 1)
	// empty_chan := make(chan interface{}, 1)
	// any_chan <- 10 //向通道写数据
	// _, ok := <- empty_chan
	// if ok{
	// 	fmt.Println("取出了数据")
	// }else{
	// 	fmt.Println("没有数据")
	// }
	// fmt.Println(<- any_chan) // 取出通道数据
	// fmt.Println(runtime.NumCPU())
	// wg.Add(2)
	// go add(mutex)
	// go minu(mutex)
	// wg.Wait()
}
