package main

import "fmt"

type stru struct {
	x int
	y int
}

//构造函数
func NewStru() *stru {
	return &stru{}
}

type B struct {
	x int
}

//A继承B
type A struct {
	x int
	b B
}

//定义结构体方法
//接收器类型是指针
func (a *A) setX(x int) {
	a.x = x
}

func (a *A) getX() (x int) {
	return a.x
}

func (a A) setXV(x int) {
	a.x = x
}

//基本类型添加方法
type Byte int

func (a Byte) isZero() (is bool) {
	return a == 0
}

//定义结构体
type class struct {
}

//定义结构体方法
func (c *class) do(a int) (ok bool) {
	ok = true
	if 0 == a {
		ok = false
	}
	return ok
}

//普通函数
func doFunc(a int) (ok bool) {
	ok = true
	if 0 == a {
		ok = false
	}
	return ok
}
func main() {

	// s := stru{
	// 	x: 10,
	// 	y: 10,
	// }

	// s1 := &stru{10, 10}

	// s2 := &stru{x: 10, y: 10}

	// s3 := new(stru)

	// // s4 := new(stru{x: 10, y: 10})

	// s5 := NewStru()

	// var a Byte = 10
	// fmt.Println(a.isZero())

	// s6 := &A{x: 6, b: B{x: 10}}

	// s6.setX(100)
	// fmt.Println(s6.x, s6.b.x)
	// fmt.Println(s6.getX())
	// // 接收器类型为值类型
	// s6.setXV(9) //对s6的更改无效
	// fmt.Println("error", s6.getX())

	//定义一个代理--相当于函数指针
	var delegate func(int) bool

	c := new(class)

	delegate = c.do

	fmt.Println(delegate(0)) //执行结构体方法

	delegate = doFunc
	fmt.Println(delegate(0)) //执行普通函数

}
