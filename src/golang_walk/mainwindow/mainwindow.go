package mainwindow

import (
	"fmt"
	"golang_walk/tcpclient"
	"strconv"

	"github.com/lxn/walk"
	. "github.com/lxn/walk/declarative"
	//"strconv"
)

type MyMainWindow struct {
	tcp *tcpclient.TcpClient
	*walk.MainWindow
	btnConnect  *walk.PushButton // 连接按钮
	btnSend     *walk.PushButton // 发送按钮
	labelServer *walk.Label      // 客户端
	lineServer  *walk.LineEdit   // 服务端ip
	labelPort   *walk.Label      // 客户端port
	linePort    *walk.LineEdit

	groupNet     *walk.GroupBox
	groupRecv    *walk.GroupBox
	groupSend    *walk.GroupBox
	recvradioHex *walk.RadioButton
	recvradioAsc *walk.RadioButton
	sendradioHex *walk.RadioButton
	sendradioAsc *walk.RadioButton
	text         *walk.TextEdit
	lineSend     *walk.LineEdit
}

func (win *MyMainWindow) CreateMainWindow() error {
	win.tcp = tcpclient.CreateTcp() // 初始化TCP sock
	// 创建主窗口
	_, err := MainWindow{
		Title: "TCP客户端",
		//MinSize:  Size{Width: 600, Height: 400},
		AssignTo: &win.MainWindow,
		Layout:   HBox{Alignment: AlignHVDefault},
		Children: []Widget{

			Composite{
				Layout: VBox{Alignment: AlignHCenterVNear},
				Children: []Widget{
					GroupBox{
						Title:    "网络",
						Layout:   VBox{},
						AssignTo: &win.groupNet,
						Children: []Widget{
							Label{
								Text:      "服务器IP",
								Alignment: AlignHNearVCenter,
							},
							LineEdit{
								AssignTo: &win.lineServer,
							},
							Label{
								AssignTo: &win.labelPort,
								Text:     "服务器端口号",
							},
							LineEdit{AssignTo: &win.linePort},
							PushButton{
								Text:    "连接",
								MinSize: Size{Height: 50, Width: 50},
								MaxSize: Size{Height: 50, Width: 50},
								OnClicked: func() {
									ip := win.lineServer.Text()
									port := win.linePort.Text()
									if 0 == len(ip) || 0 == len(port) {
										walk.MsgBox(win, "错误", "必须输入ip和port", walk.MsgBoxOK)
									} else {
										p, err := strconv.Atoi(port)
										if err != nil {
											walk.MsgBox(win, "错误", "端口号格式错误", walk.MsgBoxOK)
										}
										//win.text.AppendText("插入数据\r\r\n")
										go win.tcp.ConnectToHsot(ip, p)
										go win.recvFromTcp()
										go win.pushToText()
									}
								},
								AssignTo: &win.btnConnect,
							},
						},
					},
					GroupBox{
						AssignTo: &win.groupRecv,
						Title:    "接收设置",
						Layout:   HBox{},
						Children: []Widget{
							RadioButton{
								AssignTo: &win.recvradioAsc,
								Text:     "ASCII",
							},
							RadioButton{
								AssignTo: &win.recvradioHex,
								Text:     "HEX",
							},
						},
					},
					GroupBox{
						AssignTo: &win.groupSend,
						Title:    "发送设置",
						Layout:   HBox{},
						Children: []Widget{
							RadioButton{
								AssignTo: &win.sendradioAsc,
								Text:     "ASCII",
							},
							RadioButton{
								AssignTo: &win.sendradioHex,
								Text:     "HEX",
							},
						},
					},
				},
			},
			Composite{
				Layout: VBox{},
				Children: []Widget{
					TextEdit{
						AssignTo: &win.text,
					},
					Composite{
						Layout: HBox{},
						Children: []Widget{
							LineEdit{
								AssignTo: &win.lineSend,
								MinSize:  Size{Height: 100, Width: 500},
							},
							PushButton{
								AssignTo: &win.btnSend,
								Text:     "发送",
								MinSize:  Size{Height: 100, Width: 100},
								MaxSize:  Size{Height: 100, Width: 100},
								OnClicked: func() {
									str := win.lineSend.Text()
									win.tcp.SendData(str)
								},
							},
						},
					},
				},
			},
		},
	}.Run()
	return err
}

func (win *MyMainWindow) recvFromTcp() {
	for {
		win.tcp.RecvData()
		//if 0 != len(str) {
		//	win.tcp.Ch <- str
		//}
	}
}

func (win *MyMainWindow) pushToText() {
	defer func() {
		if err := recover(); err != nil {
			fmt.Println("err discription：", err) // 这里的err其实就是panic传入的内容
			fmt.Println("我是defer里的匿名函数，我捕获到panic的异常了，我要recover，恢复过来了。")
			return
		}
	}()
	for {
		str := <- win.tcp.Ch
		fmt.Print(str + "\n")
		if 0 != len(str) {
			win.text.AppendText(str + "\r\n")
		}
	}
}
