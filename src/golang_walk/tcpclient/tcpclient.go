package tcpclient

import (
	"fmt"
	"github.com/axgle/mahonia"
	"net"
	"strconv"
	"strings"
)

// TcpClient TCP socket客户端
type TcpClient struct {
	Ch chan string
	conn net.Conn
	ip string	// 服务器ip
	port int	// 服务器端口号
}

func CreateTcp() *TcpClient {
	// 获取主机ip
	tcp := new(TcpClient)
	tcp.ip = "192.168.10.61"
	//tcp.ip = localIP()
	tcp.port = 7171
	tcp.Ch = make(chan string, 10)
	return tcp
}

func localIP() string {
	var ip string
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		return ip
	}

	for _, a := range addrs {
		if ipnet, ok := a.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				ipp := ipnet.IP.String()
				if strings.HasPrefix(ip, "192.168") {
					ip = ipp
					break
				}
			}
		}
	}
	return ip
}

func (tcp *TcpClient) ConnectToHsot(ip string, port int) error {
	var err error
	tcp.conn, err = net.Dial("tcp", ip + ":" + strconv.Itoa(port))
	if err != nil {
		tcp.conn = nil
	} else {
		go tcp.RecvData()
	}

	return err
}

func (tcp *TcpClient) GetIP() (ip string, port int) {
	ip = tcp.ip
	port = tcp.port
	return ip, port
}

func (tcp *TcpClient) SendData(str string) {
	defer func() {
		if err := recover(); err != nil {
			fmt.Println("异常描述：", err) // 这里的err其实就是panic传入的内容
			fmt.Println("我是defer里的匿名函数，我捕获到panic的异常了，我要recover，恢复过来了。")
			return
		}
	}() //注意这个

	if 0 != len(str){
		if tcp.conn == nil {
			fmt.Print("发送连接无效\n")
		} else {
			by := []byte(str)
			tcp.conn.Write(by)
		}
	}
}

func (tcp *TcpClient) RecvData() {
	defer func() {
		if err := recover(); err != nil {
			fmt.Println("异常描述：", err) // 这里的err其实就是panic传入的内容
			fmt.Println("我是defer里的匿名函数，我捕获到panic的异常了，我要recover，恢复过来了。")
			return
		}
	}() //注意这个

	if tcp.conn != nil {
		by := make([]byte, 1024)
		num, err := tcp.conn.Read(by)
		//fmt.Print(num)
		if err != nil {
			fmt.Print("读取数据失败\n" + "\n")
		} else {
			// GBK转unicode
			str := string(by[:num])
			res := convertToString(str, "gbk", "utf-8")
			tcp.Ch <- string(res)
		}
	}
}

func convertToString(src string, srcCode string, tagCode string) string {
	srcCoder := mahonia.NewDecoder(srcCode)
	srcResult := srcCoder.ConvertString(src)
	tagCoder := mahonia.NewDecoder(tagCode)
	_, cdata, _ := tagCoder.Translate([]byte(srcResult), true)
	result := string(cdata)
	return result
}
