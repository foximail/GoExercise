module golang_walk

go 1.13

require (
	github.com/akavel/rsrc v0.8.0 // indirect
	github.com/axgle/mahonia v0.0.0-20180208002826-3358181d7394
	github.com/lxn/walk v0.0.0-20191128110447-55ccb3a9f5c1
	github.com/lxn/win v0.0.0-20191128105842-2da648fda5b4 // indirect
	golang.org/x/sys v0.0.0-20200413165638-669c56c373c4 // indirect
	gopkg.in/Knetic/govaluate.v3 v3.0.0 // indirect
)
