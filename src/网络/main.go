package main

import (
	"fmt"
	"io"
	"log"
	"net"
	"sync"
	"time"
)

var wg *sync.WaitGroup = new(sync.WaitGroup)

func main() {

	addr := net.TCPAddr{
		IP:   net.ParseIP("192.168.10.61"),
		Port: 7171,
	}

	listener, err := net.ListenTCP("tcp4", &addr)
	if err != nil {
		log.Fatal(err)
	}

	// 循环等待连接
	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println("客户端：", conn.RemoteAddr())
		wg.Add(1)
		go recv(conn)
		time.Sleep(time.Second)
	}
}

func recv(c net.Conn) {
	defer c.Close()
	for {
		buf := make([]byte, 1024)
		count, err := c.Read(buf)
		fmt.Println(count)
		if err != nil {
			if io.EOF == err {
				fmt.Println(c.RemoteAddr(), " 断开连接")
			}
			break
		} else {
			s := "接收到来自客户端：" + c.RemoteAddr().String() + " 的消息：" + string(buf[:count])
			fmt.Println(s)
		}
	}
	wg.Done()
}
