package main

import (
	"fmt"
	"包/test"
)

type A struct{}

// 实现接口1
func (a *A) Test() string {
	s := "结构体A实现接口1\n"
	return s
}

func main() {
	var inter test.Tester
	var inter1 test.Tester1 // 接口1
	a := new(A)
	inter = a
	fmt.Println(inter.Test())

	b := new(A)
	inter1 = b
	fmt.Println(inter1.Test())

	c := test.NewPerson()
	c.Name = "www"
	c.SetAge(10)
	fmt.Println(c.Name)
	fmt.Println(c.GetAge())

}
