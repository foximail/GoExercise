package test

type Tester interface {
	Test() string
}

// 此结构体不能在别的包中直接使用
type person struct {
	Name string // 公有变量
	age  int    // 私有变量
}

// 构造函数
func NewPerson() *person {
	return new(person)
}

func (a *person) SetAge(age int) {
	a.age = age
}

func (a *person) GetAge() int {
	return a.age
}
