package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	fmt.Println(os.Getpid(), " ", os.Getppid())
	var arr [10]int = [10]int{1, 2, 3}
	var slice []int = arr[0:5]
	slice = append(slice, 10)
	fmt.Println(slice)
	inputReader := bufio.NewReader(os.Stdin) // 定义一个标准输入设备变量
	input, err := inputReader.ReadString('\n')
	if err != nil {
		fmt.Println("An error")
	} else {
		input = input[:len(input)-1] // 去掉字符串行尾的换行符
		fmt.Println("Hello ", input)
	}
}
