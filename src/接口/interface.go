package main

import "fmt"

// 接口定义与实现
// type People struct {
// 	sex int //为该成员变量定义一个接口，用于别的包访问该成员变量
// }

// // 声明接口
// type Peopler interface {
// 	getSex() int
// }

// // People结构体实现接口
// func (a *People) getSex() int {
// 	return a.sex
// }

// func main() {
// 	// 实例化结构体
// 	p := new(People)
// 	// 实例化接口--接口是类型，也需要实例化
// 	var inter Peopler
// 	inter = p
// 	fmt.Println(inter.getSex())
// }

// 接口与类型的关系
// 多个互不相关的类型实现同一个接口

// Writer ...
type Writer interface {
	Write([]byte) (int, error)
}

// File ...
type File struct {
}

// Socket ...
type Socket struct {
}

func (a *Socket) Write(t []byte) (int, error) {
	return 0, nil
}

func (a *File) Write(t []byte) (int, error) {
	return 1, nil
}

// ComB ...
type ComB interface {
	f1() string
	f2() string
}

//Child ...
type Child struct {
}

// Father ...
type Father struct {
	Child
}

func (f Father) f1() string {
	return "我是父亲，我实现了f1\n"
}

func (f Child) f2() string {
	return "我是儿子，我实现了f2\n"
}

func main() {
	var data []byte
	sock := new(Socket)
	file := new(File)
	var inter Writer

	inter = sock // 接口与socket绑定，调用socket的write
	a, _ := inter.Write(data)
	fmt.Println(a)

	inter = file
	b, _ := inter.Write(data)
	fmt.Println(b)

	var com ComB
	fa := new(Father)
	com = fa
	fmt.Println(com.f1())
	fmt.Println(com.f2())

	// 接口类型断言
	// 接口类型断言，类型接收器不能是指针
	// 接口类型断言没什么意思
	_, ok := com.(Father)
	fmt.Println(ok)

	var x interface{}
	x = 10
	value, ok := x.(int)
	fmt.Println(value, "  ", ok)

}
