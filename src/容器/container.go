package main

import "fmt"

func main() {
	// 1. 数组
	// 1.1 1维数组
	// 在go中很少直接使用数组
	//默认初始化
	var arr [10]int
	fmt.Println(arr[0], arr[1], arr[3])
	//用一组值来初始化数组
	var arr1 [3]int = [3]int{1, 2}
	fmt.Println(arr1[0], arr1[1], arr1[2], len(arr1)) //在编译的时候就检查数组下标是否越界
	//让go语言来决定数组长度
	// var arr2 [3]int = [...]int{1, 2, 3}

	//错误用法
	//Go语言不支持类型的自动隐式转换，所以，不可以用
	//2个元素的数组初始化3个元素的数组，即在Go语言中，
	//数组类型由2个字段决定：数据元素个数+数据类型
	//[2]int 与 [3]int 不是同一种数据类型。在Go语言中，
	//数组的定义：[count]type，注意：[]int不是一个表达式
	//而是一个类型，所以，想显示地给一个数组所有元素初始化为0，
	//arr := [2]int这种写法是错误的
	// var arr2 [3]int = [...]int{1, 3}

	// 遍历访问数组
	arr3 := [10]int{0} //显示地给所有元素初始化为0
	for k, v := range arr3 {
		if 0 == v {
			arr3[k] = k
		}
	}
	fmt.Println(arr3)

	// 多维数组
	var mulArr [3][2]int = [3][2]int{{1, 2}, {3, 4}, {5, 6}}
	fmt.Println("修改前：")
	fmt.Println(mulArr)
	for k, v := range mulArr {
		for j, _ := range v {
			mulArr[k][j] = 1
		}
	}
	fmt.Println("修改后：")
	fmt.Println(mulArr)

	// 2. 切片
	// 切片类似与python中的列表数据类型，c++中的vector，
	// 切片是对连续片段的 **引用**，所以，修改了切片的值
	// 也就修改了原始片段的值
	// 切片中的元素是连续布局的
	// 切片是新的数据类型，不要与数组搞混淆了。切片的元素
	// 可以来自于数组，也可以来自于其他连续内存片段
	// 切片的操作与matlab中的一致

	// 2.1. 从数组生成切片
	s1 := arr3[1:5]      //切片左闭右开
	s1 = append(s1, 100) // 引用数组的切片也可以增长,并且导致原始数组也增长了
	// s2 := arr3[:]   //s2 引用arr3的所有元素
	fmt.Println(s1)
	s1[0] = 10 //对切片修改，也改变了arr3的值
	fmt.Println("修改切片后")
	fmt.Println(s1)
	fmt.Println(arr3)

	// 2.2. 直接生成切片
	var s3 []int //注意：切片是动态结构，可自由增长，不能在[]中指定元素大小，否则，切片声明变成数组声明了
	for x := 0; x < 10; x++ {
		s3 = append(s3, x)
	}
	fmt.Println("切片")
	fmt.Println(s3)

	// 2.3. 很重要: 切片拷贝 copy
	// copy函数可将切片A元素复制到切片B中
	// 如果两个内存大小不一致,将会按照较小的那个内存对象复制
	// 这个类似于c/c++中的memcpy_s函数
	// copy只能将切片元素进行拷贝
	// 对于连续内存的拷贝,可以先转化为切片,然后使用copy函数进行拷贝

	arr4 := [2]int{0}
	copy(arr4[:], arr3[:]) //错误用法: copy(arr4, arr3)
	fmt.Println(arr4)

	// 2.4. 删除切片元素
	s4 := arr3[:]
	s4 = s4[1:5]

	// 2.5. 多维切片
	s5 := mulArr[:][:]
	for k, v := range s5 {
		for j, _ := range v {
			fmt.Println(s5[k][j])
		}
	}

	//3. 列表
	// 列表是不连续内存的容器,列斯与c++中的链表list

}
