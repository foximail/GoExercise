module testwalk

go 1.13

require (
	github.com/lxn/walk v0.0.0-20191113135339-bf589de20b3c
	github.com/lxn/win v0.0.0-20191128105842-2da648fda5b4 // indirect
	golang.org/x/sys v0.0.0-20200302083256-062a44052db1 // indirect
	gopkg.in/Knetic/govaluate.v3 v3.0.0 // indirect
)
