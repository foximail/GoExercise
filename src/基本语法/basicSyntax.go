package main

import (
	"fmt"
)

func getTwo(a int) (int, int) {
	a = 1000
	return a, 200
}

func main() {
	// var a int = 100
	// var b int = 200
	// a, b = b, a //变量的多重赋值按照从左到右的顺序

	// //匿名变量_，即丢弃函数返回值，相当于matlab里的~符号
	// _, c := getTwo(a)
	// d, _ := fmt.Println(a, b, c)

	// fmt.Println(d)

	// const size int = 300 //图片大小
	// pic := image.NewGray(image.Rect(0, 0, size, size))

	// for x := 0; x < size; x++ {
	// 	for y := 0; y < size; y++ {

	// 		pic.SetGray(x, y, color.Gray{0})
	// 	}
	// }
	// //Go语言不支持数据类型之间的隐式的类型转换
	// for x := 0; x < size; x++ {

	// 	s := float64(x) * 2 * math.Pi / float64(size)
	// 	y := float64(size) * (math.Sin(s))
	// 	pic.SetGray(x, int(y), color.Gray{255})
	// }

	// //创建文件
	// file, err := os.Create("sin.png")

	// if err != nil {
	// 	log.Fatal(err)
	// }

	// //将灰度图像数据写入png文件
	// png.Encode(file, pic)

	// file.Close()

	//字符串
	//Go语言中字符串是内容不可改变的定长字节序列，这一点与python一致
	//严格来说，c/c++中没有字符串，

	//go语言的指针只是告诉你内存地址，不能做偏移运算，即不能获取内存中部分数据
	//内存地址在32位/64位机器上分别占用4字节/8字节长度

	var a int = 10
	var p *int = &a
	fmt.Println(p)  // 打印p中保存的内存地址
	fmt.Println(*p) // 取p中保存的内存地址的值

	// str := "我们都是好孩子"
	// fmt.Println("%p", &str)
	// fmt.Println("我们都是好孩子")

	var s string = "我们"
	fmt.Println(s)

	// 数组
	var arr [10]int = [10]int{10}
	fmt.Println(arr)
	for i, v := range arr {
		println("i = %d, v = %d", i, v)
	}

}
